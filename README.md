![Image with example](https://raw.githubusercontent.com/hryniuk/blender-intro/master/render.png?raw=true)

# Introduction to Blender

This repo contains all the materials I've used for my presentation about
Blender at Adam Mickiewicz University. There are a few files here:
* [`basic_tutorial.blend`](https://github.com/hryniuk/blender-intro/raw/master/basic_tutorial.blend) - tutorial that covers basic editing actions in Blender
* [`animation.blend`](https://github.com/hryniuk/blender-intro/raw/master/animation.blend) - example of animation created using Python
* [`function.blend`](https://github.com/hryniuk/blender-intro/raw/master/function.blend) - example of applying a function to a plane's vertices
* [`gen_pos.py`](https://github.com/hryniuk/blender-intro/blob/master/gen_pos.py) - script I've use to generate [`positions.json`](https://github.com/hryniuk/blender-intro/blob/master/positions.json) file used in `animation.blend`
* [`intro.blend`](https://github.com/hryniuk/blender-intro/raw/master/intro.blend) - Blender project for the scene from the image above
* [`blender-intro.tex`](https://github.com/hryniuk/blender-intro/blob/master/blender-intro.tex) - source file of the [slides from our first meeting](https://github.com/hryniuk/blender-intro/blob/master/blender-intro.pdf)
* [`python-in-blender.tex`](https://github.com/hryniuk/blender-intro/blob/master/python-in-blender.tex) - source file of the [slides from our first meeting](https://github.com/hryniuk/blender-intro/blob/master/python-in-blender.pdf)

## Things I've mentioned

* [Is Blender the future of 2D Animation? (YouTube)](https://www.youtube.com/watch?v=L1Wl3YoRe8w)
* [Tears of Steel (YouTube)](https://www.youtube.com/watch?v=R6MlUcmOul8)
* [Magnetic Fields (YouTube)](https://www.youtube.com/watch?v=2_hwGahuDRE)
* [3D Scientific Visualization with Blender](https://www.cv.nrao.edu/~bkent/blender/index.html)
* [Blender 2.8](https://www.blender.org/2-8/)

## Learning resources

### ENG

* [Application Shortcut Mapper by Waldo Bronchart](http://waldobronchart.github.io/ShortcutMapper/#Blender)
* [Blender infographic map](https://www.giudansky.com/design/51-blender-map)
* [Blender Guru tutorials](https://www.blenderguru.com/)
* [Blender.org tutorials](https://www.blender.org/support/tutorials/)
* [Official Blender/Python API Documentation](https://docs.blender.org/api/current/)

### PL

* [Polski Kurs Blendera](http://polskikursblendera.pl/)
